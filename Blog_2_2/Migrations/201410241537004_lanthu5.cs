namespace Blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lanthu5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        Password = c.String(),
                        Email = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.AccountID);
            
            AddColumn("dbo.BaiViet", "AccountID", c => c.Int(nullable: false));
            AlterColumn("dbo.BaiViet", "ID", c => c.Int(nullable: false));
            AddForeignKey("dbo.BaiViet", "AccountID", "dbo.Accounts", "AccountID", cascadeDelete: true);
            CreateIndex("dbo.BaiViet", "AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.BaiViet", new[] { "AccountID" });
            DropForeignKey("dbo.BaiViet", "AccountID", "dbo.Accounts");
            AlterColumn("dbo.BaiViet", "ID", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.BaiViet", "AccountID");
            DropTable("dbo.Accounts");
        }
    }
}
