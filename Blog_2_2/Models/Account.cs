﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Account
    {
        public int AccountID{ get; set; }
        [Required(ErrorMessage="Vui lòng nhập nội dung")]
        [DataType(DataType.Password)]
        public String Password { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [DataType(DataType.EmailAddress)]
        public String Email{ get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [StringLength(100,ErrorMessage= "Số ký tự tối đa là 100")]
        public String FirstName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [StringLength(100, ErrorMessage = "Số ký tự tối đa là 100")]
        public String LastName { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

    }
}