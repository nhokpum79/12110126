﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Comment
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [MinLength ( 50,ErrorMessage = "Số lượng ký tự tối thiểu là 50")]
        public String Body { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập thông tin hợp lệ")]
        [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập thông tin hợp lệ")]
        [DataType(DataType.Date)]
        public DateTime DateUpdated { get; set; }

        [Required(ErrorMessage="Vui lòng nhập nội dung")]
        public String Author { get; set; }

        public int PostID { get; set; }
        public virtual Post Post { get; set; }
    }
}