﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Tag
    {
        public int TagID { get; set; }

        [Required(ErrorMessage="Vui lòng nhập nội dung")]
        [StringLength(100, ErrorMessage = "Số lượng trong khoảng 10-100", MinimumLength = 10)]
        public String Content { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
       
    }
}