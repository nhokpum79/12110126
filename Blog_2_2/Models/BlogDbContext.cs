﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class BlogDbContext:DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>()
                .HasMany(d => d.Tags).WithMany(p => p.Posts)
                .Map(t => t.MapLeftKey("PostID").MapRightKey("TagID").ToTable("Tag_Post"));
        }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<Tag> Tags { get; set; }
    }
}