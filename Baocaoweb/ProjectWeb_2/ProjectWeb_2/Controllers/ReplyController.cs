﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectWeb_2.Models;

namespace ProjectWeb_2.Controllers
{
    public class ReplyController : Controller
    {
        private ProjectWeb_2DbContext db = new ProjectWeb_2DbContext();

        //
        // GET: /Reply/

        public ActionResult Index()
        {
            var replies = db.Replies.Include(r => r.Comment).Include(r => r.UserProfile);
            return View(replies.ToList());
        }

        //
        // GET: /Reply/Details/5

        public ActionResult Details(int id = 0)
        {
            Reply reply = db.Replies.Find(id);
            if (reply == null)
            {
                return HttpNotFound();
            }
            return View(reply);
        }

        //
        // GET: /Reply/Create

        public ActionResult Create()
        {
            ViewBag.CommentID = new SelectList(db.Comments, "ID", "NoiDung");
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Reply/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Reply reply)
        {
            if (ModelState.IsValid)
            {
                db.Replies.Add(reply);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CommentID = new SelectList(db.Comments, "ID", "NoiDung", reply.CommentID);
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", reply.UserProfileUserId);
            return View(reply);
        }

        //
        // GET: /Reply/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Reply reply = db.Replies.Find(id);
            if (reply == null)
            {
                return HttpNotFound();
            }
            ViewBag.CommentID = new SelectList(db.Comments, "ID", "NoiDung", reply.CommentID);
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", reply.UserProfileUserId);
            return View(reply);
        }

        //
        // POST: /Reply/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Reply reply)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reply).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CommentID = new SelectList(db.Comments, "ID", "NoiDung", reply.CommentID);
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", reply.UserProfileUserId);
            return View(reply);
        }

        //
        // GET: /Reply/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Reply reply = db.Replies.Find(id);
            if (reply == null)
            {
                return HttpNotFound();
            }
            return View(reply);
        }

        //
        // POST: /Reply/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Reply reply = db.Replies.Find(id);
            db.Replies.Remove(reply);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}