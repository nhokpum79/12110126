﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectWeb_2.Models;

namespace ProjectWeb_2.Controllers
{
    public class HuongDanController : Controller
    {
        private ProjectWeb_2DbContext db = new ProjectWeb_2DbContext();

        //
        // GET: /HuongDan/

        public ActionResult Index()
        {
            var huongdans = db.HuongDans.Include(h => h.BaiViet);
            return View(huongdans.ToList());
        }

        //
        // GET: /HuongDan/Details/5

        public ActionResult Details(int id = 0)
        {
            HuongDan huongdan = db.HuongDans.Find(id);
            if (huongdan == null)
            {
                return HttpNotFound();
            }
            return View(huongdan);
        }

        //
        // GET: /HuongDan/Create

        public ActionResult Create()
        {
            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "MoTa");
            return View();
        }

        //
        // POST: /HuongDan/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HuongDan huongdan)
        {
            if (ModelState.IsValid)
            {
                db.HuongDans.Add(huongdan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "MoTa", huongdan.BaiVietID);
            return View(huongdan);
        }

        //
        // GET: /HuongDan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            HuongDan huongdan = db.HuongDans.Find(id);
            if (huongdan == null)
            {
                return HttpNotFound();
            }
            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "MoTa", huongdan.BaiVietID);
            return View(huongdan);
        }

        //
        // POST: /HuongDan/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HuongDan huongdan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(huongdan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "MoTa", huongdan.BaiVietID);
            return View(huongdan);
        }

        //
        // GET: /HuongDan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            HuongDan huongdan = db.HuongDans.Find(id);
            if (huongdan == null)
            {
                return HttpNotFound();
            }
            return View(huongdan);
        }

        //
        // POST: /HuongDan/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HuongDan huongdan = db.HuongDans.Find(id);
            db.HuongDans.Remove(huongdan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}