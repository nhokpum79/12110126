﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectWeb_2.Models;

namespace ProjectWeb_2.Controllers
{
    public class ChuyenMucController : Controller
    {
        private ProjectWeb_2DbContext db = new ProjectWeb_2DbContext();

        //
        // GET: /ChuyenMuc/

        public ActionResult Index()
        {
            return View(db.ChuyenMucs.ToList());
        }

        //
        // GET: /ChuyenMuc/Details/5

        public ActionResult Details(int id = 0)
        {
            ChuyenMuc chuyenmuc = db.ChuyenMucs.Find(id);
            if (chuyenmuc == null)
            {
                return HttpNotFound();
            }
            return View(chuyenmuc);
        }

        //
        // GET: /ChuyenMuc/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ChuyenMuc/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ChuyenMuc chuyenmuc)
        {
            if (ModelState.IsValid)
            {
                db.ChuyenMucs.Add(chuyenmuc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(chuyenmuc);
        }

        //
        // GET: /ChuyenMuc/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ChuyenMuc chuyenmuc = db.ChuyenMucs.Find(id);
            if (chuyenmuc == null)
            {
                return HttpNotFound();
            }
            return View(chuyenmuc);
        }

        //
        // POST: /ChuyenMuc/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ChuyenMuc chuyenmuc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chuyenmuc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(chuyenmuc);
        }

        //
        // GET: /ChuyenMuc/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ChuyenMuc chuyenmuc = db.ChuyenMucs.Find(id);
            if (chuyenmuc == null)
            {
                return HttpNotFound();
            }
            return View(chuyenmuc);
        }

        //
        // POST: /ChuyenMuc/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChuyenMuc chuyenmuc = db.ChuyenMucs.Find(id);
            db.ChuyenMucs.Remove(chuyenmuc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}