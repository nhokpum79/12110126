﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectWeb_2.Models;

namespace ProjectWeb_2.Controllers
{
    [Authorize ]
    public class BaiVietController : Controller
    {
        private ProjectWeb_2DbContext db = new ProjectWeb_2DbContext();

        //
        // GET: /BaiViet/

        [AllowAnonymous ]
        public ActionResult Index()
        {
            var baiviets = db.BaiViets.Include(b => b.UserProfile).Include(b => b.LoaiBaiViet);
            return View(baiviets.ToList());
        }

        //
        // GET: /BaiViet/Details/5

        [AllowAnonymous ]
        public ActionResult Details(int id = 0)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            ViewData["idbaiviet"] = id;
            return View(baiviet);
        }

        //
        // GET: /BaiViet/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            ViewBag.LoaiBaiVietID = new SelectList(db.LoaiBaiViets, "ID", "TenLoai");
            return View();
        }

        //
        // POST: /BaiViet/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BaiViet baiviet, string loai, string loaimonan)
        {
            if (ModelState.IsValid)
            {
                if (loai == "MonAn")
                {
                    if (loaimonan == "MonMan") 
                    {
                        baiviet.LoaiBaiVietID = 1;
                    }
                }

                baiviet.NgayTao = DateTime.Now;
                baiviet.NgayChinhSua = DateTime.Now;
                baiviet.TongNguoiRat = 0;
                baiviet.TongRat = 0;
                int userid=db.UserProfiles.Select (x=> new{x.UserId ,x.UserName })
                    .Where (y=>y.UserName==User.Identity.Name).Single().UserId;

                baiviet.UserProfileUserId = userid;
                db.BaiViets.Add(baiviet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", baiviet.UserProfileUserId);
            ViewBag.LoaiBaiVietID = new SelectList(db.LoaiBaiViets, "ID", "TenLoai", baiviet.LoaiBaiVietID);
            return View(baiviet);
        }

        //
        // GET: /BaiViet/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", baiviet.UserProfileUserId);
            ViewBag.LoaiBaiVietID = new SelectList(db.LoaiBaiViets, "ID", "TenLoai", baiviet.LoaiBaiVietID);
            return View(baiviet);
        }

        //
        // POST: /BaiViet/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BaiViet baiviet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(baiviet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", baiviet.UserProfileUserId);
            ViewBag.LoaiBaiVietID = new SelectList(db.LoaiBaiViets, "ID", "TenLoai", baiviet.LoaiBaiVietID);
            return View(baiviet);
        }

        //
        // GET: /BaiViet/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            return View(baiviet);
        }

        //
        // POST: /BaiViet/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            db.BaiViets.Remove(baiviet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // View Mon an, suc khoe, kinh nghiem hay
        [AllowAnonymous]
        public ActionResult MonAn(int? id)
        {
            if (id.HasValue)
            {
                if (id.Value == 1)
                {
                    var monman = db.BaiViets.Include(b => b.UserProfile).Include(b => b.LoaiBaiViet).Where(j => j.LoaiBaiVietID == 1);
                    return View(monman.ToList());
                }
                if (id.Value == 2)
                {
                    var moncanh = db.BaiViets.Include(b => b.UserProfile).Include(b => b.LoaiBaiViet).Where(j => j.LoaiBaiVietID == 2);
                    return View(moncanh.ToList());
                }
                if (id.Value == 3)
                {
                    var monxao = db.BaiViets.Include(b => b.UserProfile).Include(b => b.LoaiBaiViet).Where(j => j.LoaiBaiVietID == 3);
                    return View(monxao.ToList());
                }
            }
            var baiviets = db.BaiViets.Include(b => b.UserProfile).Include(b => b.LoaiBaiViet).Where(j=>j.LoaiBaiViet.ChuyenMucID==1);
            return View(baiviets.ToList());
        }
        [AllowAnonymous]
        public ActionResult SucKhoe(int ?id)
        {
            if (id.HasValue)
            {

                if (id.Value == 4)
                {
                    var lamdep = db.BaiViets.Include(b => b.UserProfile).Include(b => b.LoaiBaiViet).Where(j => j.LoaiBaiVietID == 4);
                    return View(lamdep.ToList());
                }
                if (id.Value == 5)
                {
                    var giamcan = db.BaiViets.Include(b => b.UserProfile).Include(b => b.LoaiBaiViet).Where(j => j.LoaiBaiVietID == 5);
                    return View(giamcan.ToList());
                }
            }
            var baiviets = db.BaiViets.Include(b => b.UserProfile).Include(b => b.LoaiBaiViet).Where(j => j.LoaiBaiViet.ChuyenMucID == 2);
            return View(baiviets.ToList());
        }
        [AllowAnonymous]
        public ActionResult KinhNghiemHay(int ?id)
        {
            if (id.HasValue)
            {

                if (id.Value == 6)
                {
                    var lamdep = db.BaiViets.Include(b => b.UserProfile).Include(b => b.LoaiBaiViet).Where(j => j.LoaiBaiVietID == 6);
                    return View(lamdep.ToList());
                }
                if (id.Value == 7)
                {
                    var giamcan = db.BaiViets.Include(b => b.UserProfile).Include(b => b.LoaiBaiViet).Where(j => j.LoaiBaiVietID == 7);
                    return View(giamcan.ToList());
                }
            }
            var baiviets = db.BaiViets.Include(b => b.UserProfile).Include(b => b.LoaiBaiViet).Where(j => j.LoaiBaiViet.ChuyenMucID == 3);
            return View(baiviets.ToList());
        }


        public ActionResult Search(string searchtext)
        {
            var search = db.BaiViets.Where(x => x.TenBaiViet.Contains(searchtext));
            return View(search.ToList());
        }

    }
}