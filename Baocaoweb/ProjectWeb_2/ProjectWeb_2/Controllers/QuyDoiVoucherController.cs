﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectWeb_2.Models;

namespace ProjectWeb_2.Controllers
{
    public class QuyDoiVoucherController : Controller
    {
        private ProjectWeb_2DbContext db = new ProjectWeb_2DbContext();

        //
        // GET: /QuyDoiVoucher/

        public ActionResult Index()
        {
            var quydoivouchers = db.QuyDoiVouchers.Include(q => q.Voucher);
            return View(quydoivouchers.ToList());
        }

        //
        // GET: /QuyDoiVoucher/Details/5

        public ActionResult Details(int id = 0)
        {
            QuyDoiVoucher quydoivoucher = db.QuyDoiVouchers.Find(id);
            if (quydoivoucher == null)
            {
                return HttpNotFound();
            }
            return View(quydoivoucher);
        }

        //
        // GET: /QuyDoiVoucher/Create

        public ActionResult Create()
        {
            ViewBag.VoucherID = new SelectList(db.Vouchers, "ID", "TenVoucher");
            return View();
        }

        //
        // POST: /QuyDoiVoucher/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(QuyDoiVoucher quydoivoucher)
        {
            if (ModelState.IsValid)
            {
                db.QuyDoiVouchers.Add(quydoivoucher);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.VoucherID = new SelectList(db.Vouchers, "ID", "TenVoucher", quydoivoucher.VoucherID);
            return View(quydoivoucher);
        }

        //
        // GET: /QuyDoiVoucher/Edit/5

        public ActionResult Edit(int id = 0)
        {
            QuyDoiVoucher quydoivoucher = db.QuyDoiVouchers.Find(id);
            if (quydoivoucher == null)
            {
                return HttpNotFound();
            }
            ViewBag.VoucherID = new SelectList(db.Vouchers, "ID", "TenVoucher", quydoivoucher.VoucherID);
            return View(quydoivoucher);
        }

        //
        // POST: /QuyDoiVoucher/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuyDoiVoucher quydoivoucher)
        {
            if (ModelState.IsValid)
            {
                db.Entry(quydoivoucher).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.VoucherID = new SelectList(db.Vouchers, "ID", "TenVoucher", quydoivoucher.VoucherID);
            return View(quydoivoucher);
        }

        //
        // GET: /QuyDoiVoucher/Delete/5

        public ActionResult Delete(int id = 0)
        {
            QuyDoiVoucher quydoivoucher = db.QuyDoiVouchers.Find(id);
            if (quydoivoucher == null)
            {
                return HttpNotFound();
            }
            return View(quydoivoucher);
        }

        //
        // POST: /QuyDoiVoucher/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QuyDoiVoucher quydoivoucher = db.QuyDoiVouchers.Find(id);
            db.QuyDoiVouchers.Remove(quydoivoucher);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}