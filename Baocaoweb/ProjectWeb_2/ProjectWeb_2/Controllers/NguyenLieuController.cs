﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectWeb_2.Models;

namespace ProjectWeb_2.Controllers
{
    public class NguyenLieuController : Controller
    {
        private ProjectWeb_2DbContext db = new ProjectWeb_2DbContext();

        //
        // GET: /NguyenLieu/

        public ActionResult Index()
        {
            var nguyenlieux = db.NguyenLieux.Include(n => n.BaiViet);
            return View(nguyenlieux.ToList());
        }

        //
        // GET: /NguyenLieu/Details/5

        public ActionResult Details(int id = 0)
        {
            NguyenLieu nguyenlieu = db.NguyenLieux.Find(id);
            if (nguyenlieu == null)
            {
                return HttpNotFound();
            }
            return View(nguyenlieu);
        }

        //
        // GET: /NguyenLieu/Create

        public ActionResult Create()
        {
            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "MoTa");
            return View();
        }

        //
        // POST: /NguyenLieu/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NguyenLieu nguyenlieu)
        {
            if (ModelState.IsValid)
            {
                db.NguyenLieux.Add(nguyenlieu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "MoTa", nguyenlieu.BaiVietID);
            return View(nguyenlieu);
        }

        //
        // GET: /NguyenLieu/Edit/5

        public ActionResult Edit(int id = 0)
        {
            NguyenLieu nguyenlieu = db.NguyenLieux.Find(id);
            if (nguyenlieu == null)
            {
                return HttpNotFound();
            }
            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "MoTa", nguyenlieu.BaiVietID);
            return View(nguyenlieu);
        }

        //
        // POST: /NguyenLieu/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NguyenLieu nguyenlieu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nguyenlieu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "MoTa", nguyenlieu.BaiVietID);
            return View(nguyenlieu);
        }

        //
        // GET: /NguyenLieu/Delete/5

        public ActionResult Delete(int id = 0)
        {
            NguyenLieu nguyenlieu = db.NguyenLieux.Find(id);
            if (nguyenlieu == null)
            {
                return HttpNotFound();
            }
            return View(nguyenlieu);
        }

        //
        // POST: /NguyenLieu/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NguyenLieu nguyenlieu = db.NguyenLieux.Find(id);
            db.NguyenLieux.Remove(nguyenlieu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}