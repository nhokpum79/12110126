﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectWeb_2.Models;

namespace ProjectWeb_2.Controllers
{
    public class LoaiBaiVietController : Controller
    {
        private ProjectWeb_2DbContext db = new ProjectWeb_2DbContext();

        //
        // GET: /LoaiBaiViet/

        public ActionResult Index()
        {
            var loaibaiviets = db.LoaiBaiViets.Include(l => l.ChuyenMuc);
            return View(loaibaiviets.ToList());
        }

        //
        // GET: /LoaiBaiViet/Details/5

        public ActionResult Details(int id = 0)
        {
            LoaiBaiViet loaibaiviet = db.LoaiBaiViets.Find(id);
            if (loaibaiviet == null)
            {
                return HttpNotFound();
            }
            return View(loaibaiviet);
        }

        //
        // GET: /LoaiBaiViet/Create

        public ActionResult Create()
        {
            ViewBag.ChuyenMucID = new SelectList(db.ChuyenMucs, "ID", "TenChuyenMuc");
            return View();
        }

        //
        // POST: /LoaiBaiViet/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LoaiBaiViet loaibaiviet)
        {
            if (ModelState.IsValid)
            {
                db.LoaiBaiViets.Add(loaibaiviet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ChuyenMucID = new SelectList(db.ChuyenMucs, "ID", "TenChuyenMuc", loaibaiviet.ChuyenMucID);
            return View(loaibaiviet);
        }

        //
        // GET: /LoaiBaiViet/Edit/5

        public ActionResult Edit(int id = 0)
        {
            LoaiBaiViet loaibaiviet = db.LoaiBaiViets.Find(id);
            if (loaibaiviet == null)
            {
                return HttpNotFound();
            }
            ViewBag.ChuyenMucID = new SelectList(db.ChuyenMucs, "ID", "TenChuyenMuc", loaibaiviet.ChuyenMucID);
            return View(loaibaiviet);
        }

        //
        // POST: /LoaiBaiViet/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LoaiBaiViet loaibaiviet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(loaibaiviet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ChuyenMucID = new SelectList(db.ChuyenMucs, "ID", "TenChuyenMuc", loaibaiviet.ChuyenMucID);
            return View(loaibaiviet);
        }

        //
        // GET: /LoaiBaiViet/Delete/5

        public ActionResult Delete(int id = 0)
        {
            LoaiBaiViet loaibaiviet = db.LoaiBaiViets.Find(id);
            if (loaibaiviet == null)
            {
                return HttpNotFound();
            }
            return View(loaibaiviet);
        }

        //
        // POST: /LoaiBaiViet/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LoaiBaiViet loaibaiviet = db.LoaiBaiViets.Find(id);
            db.LoaiBaiViets.Remove(loaibaiviet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}