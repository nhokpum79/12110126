﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectWeb_2.Models;

namespace ProjectWeb_2.Controllers
{
    public class YeuThichController : Controller
    {
        private ProjectWeb_2DbContext db = new ProjectWeb_2DbContext();

        //
        // GET: /YeuThich/

        public ActionResult Index()
        {
            var yeuthiches = db.YeuThiches.Include(y => y.UserProfile).Include(y => y.BaiViet);
            return View(yeuthiches.ToList());
        }

        //
        // GET: /YeuThich/Details/5

        public ActionResult Details(int id = 0)
        {
            YeuThich yeuthich = db.YeuThiches.Find(id);
            if (yeuthich == null)
            {
                return HttpNotFound();
            }
            return View(yeuthich);
        }

        //
        // GET: /YeuThich/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "TenBaiViet");
            return View();
        }

        //
        // POST: /YeuThich/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(YeuThich yeuthich)
        {
            if (ModelState.IsValid)
            {
                db.YeuThiches.Add(yeuthich);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", yeuthich.UserProfileUserId);
            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "TenBaiViet", yeuthich.BaiVietID);
            return View(yeuthich);
        }

        //
        // GET: /YeuThich/Edit/5

        public ActionResult Edit(int id = 0)
        {
            YeuThich yeuthich = db.YeuThiches.Find(id);
            if (yeuthich == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", yeuthich.UserProfileUserId);
            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "TenBaiViet", yeuthich.BaiVietID);
            return View(yeuthich);
        }

        //
        // POST: /YeuThich/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(YeuThich yeuthich)
        {
            if (ModelState.IsValid)
            {
                db.Entry(yeuthich).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", yeuthich.UserProfileUserId);
            ViewBag.BaiVietID = new SelectList(db.BaiViets, "ID", "TenBaiViet", yeuthich.BaiVietID);
            return View(yeuthich);
        }

        //
        // GET: /YeuThich/Delete/5

        public ActionResult Delete(int id = 0)
        {
            YeuThich yeuthich = db.YeuThiches.Find(id);
            if (yeuthich == null)
            {
                return HttpNotFound();
            }
            return View(yeuthich);
        }

        //
        // POST: /YeuThich/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            YeuThich yeuthich = db.YeuThiches.Find(id);
            db.YeuThiches.Remove(yeuthich);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        
    }
}