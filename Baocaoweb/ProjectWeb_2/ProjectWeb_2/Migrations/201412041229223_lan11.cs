namespace ProjectWeb_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan11 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViets", "ThoiGianChuanBi", c => c.Int());
            AlterColumn("dbo.BaiViets", "TongThoiGian", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BaiViets", "TongThoiGian", c => c.DateTime(nullable: false));
            AlterColumn("dbo.BaiViets", "ThoiGianChuanBi", c => c.DateTime(nullable: false));
        }
    }
}
