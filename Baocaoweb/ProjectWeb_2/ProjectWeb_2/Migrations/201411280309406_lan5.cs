namespace ProjectWeb_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViets", "MoTa", c => c.String(nullable: false, maxLength: 300));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BaiViets", "MoTa", c => c.String(nullable: false));
        }
    }
}
