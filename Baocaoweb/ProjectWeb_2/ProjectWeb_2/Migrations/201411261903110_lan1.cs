namespace ProjectWeb_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Ho = c.String(nullable: false),
                        Ten = c.String(nullable: false),
                        SoDT = c.String(),
                        GioiTinh = c.Boolean(nullable: false),
                        NgaySinh = c.DateTime(nullable: false),
                        Email = c.String(nullable: false),
                        DiaChi = c.String(nullable: false),
                        DiemTL = c.Int(nullable: false),
                        SoBaiViet = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.QuyDoiVouchers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        HanQuyDoi = c.DateTime(nullable: false),
                        SoLuongQD = c.Int(nullable: false),
                        HinhThuc = c.String(nullable: false),
                        TinhTrang = c.Boolean(nullable: false),
                        VoucherID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Vouchers", t => t.VoucherID, cascadeDelete: true)
                .Index(t => t.VoucherID);
            
            CreateTable(
                "dbo.Vouchers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenVoucher = c.String(nullable: false, maxLength: 50),
                        GiaKM = c.Int(nullable: false),
                        GiaGoc = c.Int(nullable: false),
                        SoLuong = c.Int(nullable: false),
                        ChiTiet = c.String(nullable: false),
                        DieuKienApDung = c.String(nullable: false),
                        Hinh = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(nullable: false),
                        NgayTao = c.DateTime(nullable: false),
                        NgayChinhSua = c.DateTime(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                        BaiVietID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .ForeignKey("dbo.BaiViets", t => t.BaiVietID, cascadeDelete: true)
                .Index(t => t.UserProfileUserId)
                .Index(t => t.BaiVietID);
            
            CreateTable(
                "dbo.BaiViets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        MoTa = c.String(nullable: false),
                        ThoiGianChuanBi = c.DateTime(nullable: false),
                        TongThoiGian = c.DateTime(nullable: false),
                        Hinh = c.String(),
                        NgayTao = c.DateTime(nullable: false),
                        NgayChinhSua = c.DateTime(nullable: false),
                        TongNguoiRat = c.Int(nullable: false),
                        TongRat = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                        LoaiBaiVietID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: false)
                .ForeignKey("dbo.LoaiBaiViets", t => t.LoaiBaiVietID, cascadeDelete: true)
                .Index(t => t.UserProfileUserId)
                .Index(t => t.LoaiBaiVietID);
            
            CreateTable(
                "dbo.LoaiBaiViets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenLoai = c.String(nullable: false),
                        ChuyenMucID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ChuyenMucs", t => t.ChuyenMucID, cascadeDelete: true)
                .Index(t => t.ChuyenMucID);
            
            CreateTable(
                "dbo.ChuyenMucs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenChuyenMuc = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.YeuThiches",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserProfileUserId = c.Int(nullable: false),
                        BaiVietID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .ForeignKey("dbo.BaiViets", t => t.BaiVietID, cascadeDelete: true)
                .Index(t => t.UserProfileUserId)
                .Index(t => t.BaiVietID);
            
            CreateTable(
                "dbo.Ratings",
                c => new
                    {
                        UserProfileUserId = c.Int(nullable: false),
                        BaiVietID = c.Int(nullable: false),
                        RatingValue = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserProfileUserId, t.BaiVietID })
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .ForeignKey("dbo.BaiViets", t => t.BaiVietID, cascadeDelete: true)
                .Index(t => t.UserProfileUserId)
                .Index(t => t.BaiVietID);
            
            CreateTable(
                "dbo.HuongDans",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(),
                        BaiVietID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BaiViets", t => t.BaiVietID, cascadeDelete: true)
                .Index(t => t.BaiVietID);
            
            CreateTable(
                "dbo.NguyenLieux",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        STT = c.Int(nullable: false),
                        DonVi = c.Int(nullable: false),
                        TenNguyenLieu = c.String(),
                        BaiVietID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BaiViets", t => t.BaiVietID, cascadeDelete: true)
                .Index(t => t.BaiVietID);
            
            CreateTable(
                "dbo.Replies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(nullable: false),
                        NgayTao = c.DateTime(nullable: false),
                        NgayChinhSua = c.DateTime(nullable: false),
                        CommentID = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Comments", t => t.CommentID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: false)
                .Index(t => t.CommentID)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.QuyDoiVoucherUserProfiles",
                c => new
                    {
                        QuyDoiVoucher_ID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.QuyDoiVoucher_ID, t.UserProfile_UserId })
                .ForeignKey("dbo.QuyDoiVouchers", t => t.QuyDoiVoucher_ID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId, cascadeDelete: true)
                .Index(t => t.QuyDoiVoucher_ID)
                .Index(t => t.UserProfile_UserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.QuyDoiVoucherUserProfiles", new[] { "UserProfile_UserId" });
            DropIndex("dbo.QuyDoiVoucherUserProfiles", new[] { "QuyDoiVoucher_ID" });
            DropIndex("dbo.Replies", new[] { "UserProfileUserId" });
            DropIndex("dbo.Replies", new[] { "CommentID" });
            DropIndex("dbo.NguyenLieux", new[] { "BaiVietID" });
            DropIndex("dbo.HuongDans", new[] { "BaiVietID" });
            DropIndex("dbo.Ratings", new[] { "BaiVietID" });
            DropIndex("dbo.Ratings", new[] { "UserProfileUserId" });
            DropIndex("dbo.YeuThiches", new[] { "BaiVietID" });
            DropIndex("dbo.YeuThiches", new[] { "UserProfileUserId" });
            DropIndex("dbo.LoaiBaiViets", new[] { "ChuyenMucID" });
            DropIndex("dbo.BaiViets", new[] { "LoaiBaiVietID" });
            DropIndex("dbo.BaiViets", new[] { "UserProfileUserId" });
            DropIndex("dbo.Comments", new[] { "BaiVietID" });
            DropIndex("dbo.Comments", new[] { "UserProfileUserId" });
            DropIndex("dbo.QuyDoiVouchers", new[] { "VoucherID" });
            DropForeignKey("dbo.QuyDoiVoucherUserProfiles", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.QuyDoiVoucherUserProfiles", "QuyDoiVoucher_ID", "dbo.QuyDoiVouchers");
            DropForeignKey("dbo.Replies", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.Replies", "CommentID", "dbo.Comments");
            DropForeignKey("dbo.NguyenLieux", "BaiVietID", "dbo.BaiViets");
            DropForeignKey("dbo.HuongDans", "BaiVietID", "dbo.BaiViets");
            DropForeignKey("dbo.Ratings", "BaiVietID", "dbo.BaiViets");
            DropForeignKey("dbo.Ratings", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.YeuThiches", "BaiVietID", "dbo.BaiViets");
            DropForeignKey("dbo.YeuThiches", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.LoaiBaiViets", "ChuyenMucID", "dbo.ChuyenMucs");
            DropForeignKey("dbo.BaiViets", "LoaiBaiVietID", "dbo.LoaiBaiViets");
            DropForeignKey("dbo.BaiViets", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.Comments", "BaiVietID", "dbo.BaiViets");
            DropForeignKey("dbo.Comments", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.QuyDoiVouchers", "VoucherID", "dbo.Vouchers");
            DropTable("dbo.QuyDoiVoucherUserProfiles");
            DropTable("dbo.Replies");
            DropTable("dbo.NguyenLieux");
            DropTable("dbo.HuongDans");
            DropTable("dbo.Ratings");
            DropTable("dbo.YeuThiches");
            DropTable("dbo.ChuyenMucs");
            DropTable("dbo.LoaiBaiViets");
            DropTable("dbo.BaiViets");
            DropTable("dbo.Comments");
            DropTable("dbo.Vouchers");
            DropTable("dbo.QuyDoiVouchers");
            DropTable("dbo.UserProfile");
        }
    }
}
