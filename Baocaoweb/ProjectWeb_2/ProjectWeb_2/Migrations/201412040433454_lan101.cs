namespace ProjectWeb_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan101 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Profiles", name: "ID", newName: "UserProfileUserId");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.Profiles", name: "UserProfileUserId", newName: "ID");
        }
    }
}
