namespace ProjectWeb_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaiViets", "TenBaiViet", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BaiViets", "TenBaiViet");
        }
    }
}
