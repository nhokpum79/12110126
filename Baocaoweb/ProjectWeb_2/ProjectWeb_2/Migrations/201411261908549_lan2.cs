namespace ProjectWeb_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.UserProfile", "Ho");
            DropColumn("dbo.UserProfile", "Ten");
            DropColumn("dbo.UserProfile", "SoDT");
            DropColumn("dbo.UserProfile", "GioiTinh");
            DropColumn("dbo.UserProfile", "NgaySinh");
            DropColumn("dbo.UserProfile", "Email");
            DropColumn("dbo.UserProfile", "DiaChi");
            DropColumn("dbo.UserProfile", "DiemTL");
            DropColumn("dbo.UserProfile", "SoBaiViet");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfile", "SoBaiViet", c => c.Int(nullable: false));
            AddColumn("dbo.UserProfile", "DiemTL", c => c.Int(nullable: false));
            AddColumn("dbo.UserProfile", "DiaChi", c => c.String(nullable: false));
            AddColumn("dbo.UserProfile", "Email", c => c.String(nullable: false));
            AddColumn("dbo.UserProfile", "NgaySinh", c => c.DateTime(nullable: false));
            AddColumn("dbo.UserProfile", "GioiTinh", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserProfile", "SoDT", c => c.String());
            AddColumn("dbo.UserProfile", "Ten", c => c.String(nullable: false));
            AddColumn("dbo.UserProfile", "Ho", c => c.String(nullable: false));
        }
    }
}
