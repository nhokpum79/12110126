namespace ProjectWeb_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan9 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Profiles",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Ho = c.String(nullable: false),
                        Ten = c.String(nullable: false),
                        SoDT = c.String(),
                        GioiTinh = c.Boolean(nullable: false),
                        NgaySinh = c.DateTime(nullable: false),
                        Email = c.String(nullable: false),
                        DiaChi = c.String(nullable: false),
                        DiemSuDung = c.Int(nullable: false),
                        DiemHienTai = c.Int(nullable: false),
                        SoBaiViet = c.Int(nullable: false),
                        NgayTaoTaiKhoan = c.DateTime(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.ID)
                .Index(t => t.ID);
            
            AddColumn("dbo.UserProfile", "ProfileID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropIndex("dbo.Profiles", new[] { "ID" });
            DropForeignKey("dbo.Profiles", "ID", "dbo.UserProfile");
            DropColumn("dbo.UserProfile", "ProfileID");
            DropTable("dbo.Profiles");
        }
    }
}
