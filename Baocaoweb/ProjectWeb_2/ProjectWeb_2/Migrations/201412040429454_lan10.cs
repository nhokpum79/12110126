namespace ProjectWeb_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan10 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Profiles", "Ho", c => c.String());
            AlterColumn("dbo.Profiles", "Ten", c => c.String());
            AlterColumn("dbo.Profiles", "Email", c => c.String());
            AlterColumn("dbo.Profiles", "DiaChi", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Profiles", "DiaChi", c => c.String(nullable: false));
            AlterColumn("dbo.Profiles", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Profiles", "Ten", c => c.String(nullable: false));
            AlterColumn("dbo.Profiles", "Ho", c => c.String(nullable: false));
        }
    }
}
