﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectWeb_2.Models
{
    public class ChuyenMuc
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public String TenChuyenMuc { get; set; }

        public virtual ICollection<LoaiBaiViet> LoaiBaiViets { get; set; }
    }
}