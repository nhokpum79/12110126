﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectWeb_2.Models
{
    public class LoaiBaiViet
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public String TenLoai { get; set; }

        public int ChuyenMucID { get; set; }
        public virtual ChuyenMuc ChuyenMuc { get; set; }

        public virtual ICollection<BaiViet> BaiViets { get; set; }
    }
}