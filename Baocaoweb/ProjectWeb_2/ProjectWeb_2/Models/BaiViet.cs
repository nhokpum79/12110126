﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectWeb_2.Models
{
    public class BaiViet
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public String TenBaiViet { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [MaxLength(300, ErrorMessage = "Số lượng ký tự tối đa là 300")]
        public String MoTa { get; set; }

        //[Required(ErrorMessage = "Vui lòng nhập thông tin hợp lệ")]
   
        public int? ThoiGianChuanBi { get; set; }

        //[Required(ErrorMessage = "Vui lòng nhập thông tin hợp lệ")]
  
        public int? TongThoiGian { get; set; }

        public string? NguyenLieu { get; set; }

        public string? HuongDan { get; set; }

        [DataType(DataType.ImageUrl)]
        public String Hinh { get; set; }


        [DataType(DataType.Date)]
        public DateTime NgayTao { get; set; }

        [DataType(DataType.Date)]
        public DateTime NgayChinhSua { get; set; }

        public int TongNguoiRat { get; set; }

        public int TongRat { get; set; }

        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }

        public int LoaiBaiVietID { get; set; }
        public virtual LoaiBaiViet LoaiBaiViet { get; set; }

        public virtual ICollection<YeuThich> YeuThichs { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}