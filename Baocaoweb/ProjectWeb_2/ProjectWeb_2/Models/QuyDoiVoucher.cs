﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectWeb_2.Models
{
    public class QuyDoiVoucher
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [DataType(DataType.Date)]
        public DateTime HanQuyDoi { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public int SoLuongQD { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public String HinhThuc { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public Boolean TinhTrang { get; set; }

        public int VoucherID { get; set; }
        public virtual Voucher Voucher { get; set; }

        public virtual ICollection<UserProfile> UserProfiles { get; set; }
    }
}