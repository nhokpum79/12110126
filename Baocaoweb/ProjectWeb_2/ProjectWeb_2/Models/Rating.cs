﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectWeb_2.Models
{
    public class Rating
    {
        public int RatingValue { get; set; }

        [Key]
        [Column(Order = 0)]
        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }

        [Key]
        [Column(Order = 1)]
        public int BaiVietID { get; set; }
        public virtual BaiViet BaiViet { get; set; }
    }
}