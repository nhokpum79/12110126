﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectWeb_2.Models
{
    public class YeuThich
    {
        public int ID { get; set; }

        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }

        public int BaiVietID { get; set; }
        public virtual BaiViet BaiViet { get; set; }
    }
}