﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectWeb_2.Models
{
    public class Comment
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public String NoiDung { get; set; }

        //[Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [DataType(DataType.Date)]
        public DateTime NgayTao { get; set; }

        //[Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [DataType(DataType.Date)]
        public DateTime NgayChinhSua { get; set; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - NgayTao).Minutes;
            }
        }

        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }

        public int BaiVietID { get; set; }
        public virtual BaiViet BaiViet { get; set; }

    }
}