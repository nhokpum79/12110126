﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectWeb_2.Models
{
    public class Profile
    {

   //     [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public String Ho { get; set; }

     //   [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public String Ten { get; set; }

    //    [DataType(DataType.PhoneNumber)]
        public String SoDT { get; set; }

     //   [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public Boolean GioiTinh { get; set; }

     //   [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [DataType(DataType.Date)]
        public DateTime NgaySinh { get; set; }

     //   [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }

    //    [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public String DiaChi { get; set; }

      
        public int DiemSuDung { get; set; }
        public int DiemHienTai { get; set; }
        public int SoBaiViet { get; set; }
        [DataType(DataType.Date)]
        public DateTime NgayTaoTaiKhoan { get; set; }
        [Key ]
        [ForeignKey("UserProfile")]
        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}