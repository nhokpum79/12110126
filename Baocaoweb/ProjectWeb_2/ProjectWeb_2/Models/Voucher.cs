﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectWeb_2.Models
{
    public class Voucher
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [MaxLength(50, ErrorMessage = "Số lượng ký tự tối đa là 50")]
        public String TenVoucher { get; set; }

        public int GiaKM { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public int GiaGoc { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public int SoLuong { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public String ChiTiet { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        public String DieuKienApDung { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [DataType(DataType.ImageUrl)]
        public String Hinh { get; set; }

        public virtual ICollection<QuyDoiVoucher> QuyDoiVouchers { get; set; }
    }
}