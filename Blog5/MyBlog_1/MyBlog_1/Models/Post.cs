﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyBlog_1.Models
{
    public class Post
    {

       //[Range(2,100)]
       //[DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None )]
        public int ID {get; set;}

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        //[StringLength(500, ErrorMessage = "Số ký tự tối đa trong khoảng 20 - 500", MinimumLength = 20)]
        public String Title { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        //[MinLength(50, ErrorMessage = "Số lượng ký tự tối thiểu là 50")]
        public String Body { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập thông tin hợp lệ")]
        [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập thông tin hợp lệ")]
        [DataType(DataType.Date)]
        public DateTime DateUpdated { get; set; }

    
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }

        //public int AccountID { get; set; }

        //public virtual Account Account { get; set; }

        public virtual UserProfile UserProfile { get; set; }
        public int UserProfileUserId { get; set; }
    }
    
}